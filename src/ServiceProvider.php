<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni;

use Pusaka\Tanur\Support\TanurServiceProvider;

/**
 * ServiceProvider
 */
class ServiceProvider extends TanurServiceProvider
{
    /**
     * boot
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Get provided services for deferred service provider
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
