<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Repositories;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface ResourceEntityRepository
 * @package Pusaka\Geni
 */
interface ResourceEntityRepository
{
    /**
     * Save a new entity in repository
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * Retrieve all data of repository
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function retrieve($columns = ['*']);

    /**
     * Find data by key
     *
     * @param       $key
     * @param array $columns
     *
     * @return mixed
     */
    public function find($key, $columns = ['*']);

    /**
     * Update a entity in repository by key
     *
     * @param array $attributes
     * @param       $key
     *
     * @return mixed
     */
    public function update(array $attributes, $key);

    /**
     * Delete a entity in repository by key
     *
     * @param $key
     *
     * @return int
     */
    public function delete($key);
}
