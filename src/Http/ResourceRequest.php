<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Http;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest as BaseRequest;
use Illuminate\Validation\Rule;
use Pusaka\Geni\Contracts\ResourceUtilizable;
use Pusaka\Geni\Contracts\ValidateableModel;
use Pusaka\Geni\Exceptions\ResourceRequestTypeException;
use Pusaka\Geni\Exceptions\ValidationException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

/**
 * ResourceRequest
 */
class ResourceRequest extends BaseRequest
{
    protected $model;

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        return $this->getPayload();
    }

    /**
     * extract payload from the request
     *
     * @param  array $key     list
     * @param  array $default default value
     * @return mixed
     */
    final public function getPayload($onlyKeys = '*', $default = [])
    {
        $availablePayload = $this->json('data', $default);

        if (is_string($onlyKeys)) {
            return ($onlyKeys == '*') ?
                $availablePayload : array_get($availablePayload, $onlyKeys);
        }

        return array_only($availablePayload, $onlyKeys);
    }

    public function prepareForValidation()
    {
        $this->makeResource();
    }

    protected function makeResource()
    {
        $typeKey = $this->getValidResourceType();

        try {
            $this->model = $typeKey ? app()->make($typeKey) : null;
        }
        catch (\ReflectionException $e) {
            throw new ResourceRequestTypeException($typeKey);
        }
    }

    protected function getValidResourceType()
    {
        if (! ($this instanceof ResourceUtilizable)) {
            return;
        }

        $desiredTypeKey = $this->getTypeKey();
        $typeKey = $this->getPayload('type');

        if (! $typeKey) {
            throw new BadRequestHttpException('Malformed request body, data.type is required.');
        }

        if ($desiredTypeKey !== $typeKey) {
            throw new ConflictHttpException('Resource object’s type mismatch.');
        }

        return $typeKey;
    }

    public function rules()
    {
        $rules = [];

        if ($this->model instanceof ValidateableModel) {
            $mixedRules = $this->model->validationRules();
            $rules = $this->parseRulesByContext($mixedRules);
        }

        return $this->rulesByVerb($rules);
    }

    public function parseRulesByContext($rules)
    {
        $context = $this->getContext();
        $contextRules = [];

        foreach ($rules as $field => $rule) {
            preg_match('~(?P<field>[0-9a-zA-Z\-_]+)(?P<contextual>@(?P<context>create|update))?~', $field, $match);

            if (!isset($match['contextual']) || $match['context'] == $this->getContext()) {
                $contextRules[$match['field']] = $rule;
            }
        }

        return $contextRules;
    }

    public function rulesByVerb($defaultRules = [])
    {
        $methodContextRules = 'rulesFor'.studly_case($this->getContext());
        $contextRules = [];

        if (method_exists($this, $methodContextRules)) {
            $contextRules = $this->{$methodContextRules}();
        }

        $rules = array_merge($defaultRules, $contextRules);

        return $this->toJsonApiRules($rules);
    }

    public function toJsonApiRules($rules)
    {
        if ($this->header('Content-Type') != 'application/vnd.api+json') {
            return $rules;
        }

        $jsonRules = [];

        if ($this->getContext() == 'create') {
            $jsonRules['id'] = [
                Rule::unique($this->model->getTable, $this->model->getRouteKeyName())
            ];
        }
        else {
            $jsonRules['id'] = ['required'];
        }

        foreach ($rules as $field => $rule) {
            $jsonRules['attributes.'.$field] = $rule;
        }

        return $jsonRules;
    }

    protected function getContext()
    {
        $verbContexts = [
            self::METHOD_POST => 'create',
            self::METHOD_PATCH => 'update'
        ];

        return $verbContexts[$this->method()];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator))
                    ->errorBag($this->errorBag)
                    ->redirectTo($this->getRedirectUrl());
    }
}
