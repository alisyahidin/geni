<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Http;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use League\Fractal\TransformerAbstract;
use Pusaka\Geni\Contracts\ResourceUtilizable;

/**
 * JsonApiController
 */
abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs;

    protected $repository;

    protected $transformer;

    protected $typeKey;

    public function withRepository(ResourceUtilizable $repository)
    {
        $this->repository = $repository;
        $this->typeKey = $this->repository->getTypeKey();

        return $this;
    }

    public function withTransformer(TransformerAbstract $transformer)
    {
        $this->transformer = $transformer;

        return $this;
    }
}
