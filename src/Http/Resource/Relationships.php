<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Http\Resources;

use Illuminate\Container\Container;
use Illuminate\Http\Resources\MergeValue;
use Illuminate\Http\Resources\MissingValue;
use Illuminate\Support\Collection;
use Pusaka\Geni\Contracts\ResourceUtilizable;

/**
 * Relationships
 */
trait Relationships
{
    protected $includes;

    public function getIncludes($request)
    {
        if ($this->collection instanceof Collection) {
            $this->includes = new Collection;

            foreach ($this->collection as $resource) {
                $resource->prepareRelatedData($request);
                $this->includes->push($resource->getIncludes($request));
            }
        }

        return $this->includes ?
            $this->includes->flatten()->unique()
            : new MissingValue;
    }

    public function prepareRelatedData($request)
    {
        $request = $request ?: Container::getInstance()->make('request');
        $needToIncludes = explode(',', $request->get('include'));

        if (! count($needToIncludes)) {
            return;
        }

        $this->includes = new Collection;
        foreach ($needToIncludes as $relationship) {
            $related = $this->resource->{$relationship};

            if ($related) {
                $this->includeRelated($relationship, $related);
            }
        }
    }

    protected function includeRelated($relationship, $related)
    {
        if ($related instanceof Collection) {
            return $this->includeRelatedCollection($relationship, $related);
        }

        $relationResourceClass = $this->availableIncludes[$relationship];
        $relationResource = new $relationResourceClass($related);

        $this->includes->push($relationResource);
    }

    protected function includeRelatedCollection($relationship, $collection)
    {
        foreach ($collection as $related) {
            $this->includeRelated($relationship, $related);
        }
    }

    public function getRelationships(array $relationships = null, $parentLink = null)
    {
        $relCollection = new Collection;

        foreach ($relationships as $relationship) {
            if (! $this->resource->relationLoaded($relationship)) {
                continue;
            }

            $relatedObject = $this->resource->{$relationship};
            $relationshipResource = $this->relationship($relatedObject, $relationship, $parentLink);
            $relCollection->put($relationship, $relationshipResource);
        }

        if ($relCollection->count()) {
            $value = new MergeValue([
                'relationships' => $relCollection
            ]);
        }
        else {
            $value = new MissingValue;
        }

        return $value;
    }

    public function relationship($related, $relationship, $parentLink = null)
    {
        if (($related instanceof Collection) || ($related instanceof ResourceUtilizable)) {
            $data = new AnonymousRelationship($related, $relationship, $parentLink);
        }
        else {
            $data = new MissingValue;
        }

        return $data;
    }
}
