<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AnonymousIdentifier extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => $this->getTypeKey(),
            'id' => $this->getRouteKey(),
        ];
    }
}
