<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Collection;

class AnonymousRelationship extends Resource
{
    protected $parentLink;
    protected $relationship;

    public function __construct($resource, $relationship, $parentLink = null)
    {
        $this->resource = $resource;
        $this->relationship = $relationship;
        $this->parentLink = $parentLink;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->resource instanceof Collection) {
            $data = AnonymousIdentifier::collection($this->resource);
        }
        else {
            $data = new AnonymousIdentifier($this->resource);
        }

        return [
            'data' => $data,
            $this->mergeWhen($this->parentLink, [
                'links' => [
                    'self' => $this->parentLink.'/relationships/'.$this->relationship,
                    'related' => $this->parentLink.'/'.$this->relationship,
                ]
            ])
        ];
    }
}
