<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Repositories;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

/**
 * Class EloquentRelationshipsRepository
 * @package Pusaka\Geni
 */
trait EloquentRelationshipRepository
{
    protected $relations = [];

    public function setRelationships(array $relations)
    {
        $this->relations = $relations;

        return $this;
    }

    public function getRelationRouteKey($relation)
    {
        foreach ($relation['data'] as $key => $value) {
            if (is_array($value)) {
                $uuid[] = $value['id'] ?? $this->storeData($value)->uuid->toString();
            } else {
                return $relation['data']['id'] ?? $this->storeData($relation['data'])->uuid->toString();
            }
        }
        return $uuid;
    }

    public function storeData($data)
    {
        $modelRepo = app()->make('repo.'.$data['type']);
        $model = app()->make($data['type']);
        $newModel = new $model;

        unset($data['type']);

        if (isset($data['relation'])) {
            $relationData = $this->getRelationData($modelRepo, $data['relation']);
            unset($data['relation']);
            foreach ($relationData as $relation) {
                $newModel->{$relation['name']}()->associate($relation['model']);
            }
        }

        $newModel->fill($data);
        $newModel->save();

        return $newModel;
    }

    public function getRelationData($modelRepo, $relation)
    {
        $relationData = [];
        foreach ($relation as $relationName => $id) {
            $type = $modelRepo->relationships[$relationName];
            $relationModel = app()->make('repo.'.$type)->find($id);
            $relationData[] = [
                'name' => $relationName,
                'model' => $relationModel
            ];
        }
        return $relationData;
    }

    public function getRelationTypeKey($data)
    {
        foreach ($data['data'] as $key => $value) {
            if (is_array($value)) {
                return $value['type'];
            } else {
                return $value;
            }
        }
    }

    public function relatingModel(array $relationships, array $relationModel, $method)
    {
        foreach ($relationships as $relation => $data) {
            $typeKey = $this->getRelationTypeKey($relationships[$relation]);

            $this->prepareRelating($data, $this->relations[$relation]);

            $modelRelation = $this->model->$relation();
            $relatedRepo = app()->make('repo.'.$typeKey);

            if ($modelRelation instanceof $relationModel[0]) {
                $routeKey = $this->getRelationRouteKey($relationships[$relation]);
                $relatedModel = $relatedRepo->find($routeKey);
                $modelRelation->$method($relatedModel);
            }

            if ($modelRelation instanceof $relationModel[1]) {
                foreach ($this->getRelationRouteKey($relationships[$relation]) as $routeKey) {
                    $relatedModel = $relatedRepo->find($routeKey);
                    $modelRelation->$method($relatedModel);
                }
            }
        }
    }

    public function relatingModelBelongsTo(array $relationships)
    {
        $this->relatingModel($relationships, [BelongsTo::class, BelongsToMany::class], 'associate');
    }

    public function relatingModelHas(array $relationships)
    {
        $this->relatingModel($relationships, [HasOne::class, HasMany::class], 'save');
    }

    public function prepareRelating($data, $typeKey)
    {
        $relationTypeKey = $this->getRelationTypeKey($data);
        if (is_array($relationTypeKey)) {
            foreach ($relationTypeKey as $index => $requestTypeKey) {
                if ($requestTypeKey !== $typeKey) {
                    throw new ConflictHttpException("Relationships resource object’s type mismatch.");
                }
            }
        } else {
            if ($relationTypeKey !== $typeKey) {
                throw new ConflictHttpException("Relationships resource object’s type mismatch.");
            }
        }
    }
}
