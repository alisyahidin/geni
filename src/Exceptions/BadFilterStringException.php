<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Exceptions;

/**
 * BadFilterStringException
 */
class BadFilterStringException extends \Exception
{
    protected $filterStr;

    public function __construct($filterStr)
    {
        parent::__construct('Bad format filter string.');
        
        $this->filterStr = $filterStr;
    }

    public function getFilterStr()
    {
        return $this->filterStr;
    }
}
