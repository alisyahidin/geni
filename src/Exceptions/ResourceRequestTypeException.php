<?php /*** Bismillahirrahmanirrahim ***/
namespace Pusaka\Geni\Exceptions;

/**
 * ResourceRequestTypeException
 */
class ResourceRequestTypeException extends \Exception
{
    /**
     * summary
     */
    public function __construct($typeKey)
    {
        $message = "Can't make model from unbinded resource '{$typeKey}'.";

        parent::__construct($message);
    }
}
