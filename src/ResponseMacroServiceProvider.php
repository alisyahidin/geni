<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\ServiceProvider;
use Spatie\Fractalistic\Fractal;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param  ResponseFactory  $factory
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        $factory->macro('resource', function ($resource = null, $statusCode) use ($factory) {
            if ($resource instanceof Resource) {
                return $resource->response()
                    ->setStatusCode($statusCode)
                    ->header('Content-Type', 'application/vnd.api+json');
            }
            elseif ($resource instanceof Fractal) {
                return $resource->respond($statusCode, [
                        'Content-Type' => 'application/vnd.api+json',
                    ]);
            }
            else {
                return $factory->json($resource, $statusCode);
            }
        });

        $factory->macro('ok', function ($resource = null) use ($factory) {
            return $factory->resource($resource, 200);
        });

        $factory->macro('created', function ($resource = null) use ($factory) {
            return $factory->resource($resource, 201);
        });

        $factory->macro('accepted', function ($resource = null) use ($factory) {
            return $factory->resource($resource, 202);
        });
        
        $factory->macro('noContent', function () use ($factory) {
            return $factory->resource(null, 204);
        });
    }
}
