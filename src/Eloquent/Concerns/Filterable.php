<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Eloquent\Concerns;

use Pusaka\Geni\Exceptions\BadFilterStringException;
use Pusaka\Geni\Exceptions\InvalidFilterFieldException;

/**
 * Filterable
 */
trait Filterable
{
    /**
     * operator untuk filter
     *
     * @var     array
     */
    protected $allowedOperators = [
        'eq', 'neq', 'gt', 'gte', 'lt', 'lte',
        'starts_with', 'contains', 'ends_with',
        'in', 'not_in', 'between', 'not_between',
        'is_null', 'is_not_null',
        'filter',
    ];

    protected $filterConditions = [];

    /**
     * Mendifinisikan daftar field yg memungkinkan utk penyaringan
     * <code>
     * return [
     *     'name', // all operator are valid
     *     'approved' => ['eq', 'between', 'not_between']
     * ];
     * </code>
     *
     * @return     array
     */
    protected function filterableFields()
    {
        return [];
    }

    /**
     * Digunakan untuk nama colom yg disamarkan
     * <code>
     * return [
     *     'name' => 'owner.name',
     *     'approved' => 'approved_at'
     * ];
     * </code>
     * 
     * @return array
     */
    protected function pathAliases()
    {
        return [];
    }

    public function addFilter($filter)
    {
        $filter = (array)$filter;

        if (empty($this->filterableFields()) || empty($filter)) {
            return $this;
        }

        foreach ($filter as $filterStr) {
            $condition = $this->parseFilterString($filterStr);

            array_push($this->filterConditions, $condition);
        }

        return $this;
    }

    protected function parseFilterString($string)
    {
        $validOprs = implode('|', $this->allowedOperators);
        $allOpsPattern = "~^(?P<field>[a-z\d\-_\.]+)\s+(?P<operator>{$validOprs})\s+(?P<value>.+)$~i";
        $nullOpsPattern = '~^(?P<field>[a-z\d\-_\.]+)\s+(?P<operator>is_null|is_not_null)$~i';

        if (preg_match($nullOpsPattern, $string, $matches)) {
            $matches['value'] = null;
        }
        elseif (! preg_match($allOpsPattern, $string, $matches)) {
            throw new BadFilterStringException($string);
        }

        if (! in_array($matches[1], $this->filterableFields())) {
            throw new InvalidFilterFieldException($matches[1]);
        }

        // TODO: limit allowed operator by field
        return [
            'field' => $matches['field'],
            'path' => $this->interpretPath($matches['field']),
            'operator' => $matches['operator'],
            'value' => $this->interpretValue($matches['value']),
        ];
    }

    protected function interpretPath($path)
    {
        $pathAliases = $this->pathAliases();

        if (array_key_exists($path, $pathAliases)) {
            $path = $pathAliases[$path];
        }

        return $path;
    }

    protected function interpretValue($value)
    {
        $decodedValue = json_decode($value);

        if (! is_null($decodedValue)) {
            $value = $decodedValue;
        }

        return $value;
    }

    public function getFilterConditions()
    {
        return $this->filterConditions;
    }

    public function applyCondition($query, $condition)
    {
        if (! $condition) {
            return $query;
        }

        if (count($condition) && ! isset($condition['path'])) {
            foreach ($condition as $c) {
                $query = $this->applyCondition($query, $c);
            }

            return $query;
        }

        $field = $condition['field'];
        $path = $condition['path'];
        $operator = $condition['operator'];
        $value = (array)$condition['value'];

        if (in_array($operator, ['eq', 'neq', 'gt', 'gte', 'lt', 'lte'])) {
            return $this->compareConstrain($query, $path, $operator, $value);
        }
        elseif (in_array($operator, ['starts_with', 'contains', 'ends_with'])) {
            return $this->searchStringConstrain($query, $path, $operator, $value);
        }
        elseif (in_array($operator, ['in', 'not_in', 'between', 'not_between'])) {
            return $this->inSetConstrain($query, $path, $operator, $value);
        }
        elseif (in_array($operator, ['is_null', 'is_not_null'])) {
            return $this->nullConstrain($query, $path, $operator);
        }
        elseif ($operator == 'filter') {
            $studlyField = studly_case($field);
            $operationFunc = "apply{$studlyField}FilterConstrain";

            return call_user_func_array([$this, $operationFunc], [$query, $value]);
        }

        return call_user_func_array(
            [$this, $operationFunc],
            [$query, $value]
        );
    }

    protected function compareConstrain($query, $column, $operator, $values)
    {
        $comparationSymbol = [
            'eq' => '=',
            'neq' => '<>',
            'gt' => '>',
            'gte' => '>=',
            'lt' => '<',
            'lte' => '<='
        ];
        $operator = $comparationSymbol[$operator];
        $pathList = explode('.', $column);
        $column = array_pop($pathList);
        $relation = implode('.', $pathList);

        if ($relation) {
            if (count($values) > 1) {
                return $query->whereHas(
                    $relation,
                    function ($query) use ($column, $operator, $values) {
                        foreach ($values as $i => $v) {
                            $query->where($column, $operator, $v, $i > 0 ? 'or' : 'and');
                        }
                    }
                );
            }
            else {
                return $query->whereHas($relation, 
                    function ($query) use ($column, $operator, $values) {
                        $query->where($column, $operator, $values[0]);
                    });
            }
        }

        if (count($values) > 1) {
            return $query->where(function ($query) use ($column, $operator, $values) {
                foreach ($values as $i => $v) {
                    $query->where($column, $operator, $v, $i > 0 ? 'or' : 'and');
                }
            });
        }
        else {
            return $query->where($column, $operator, $values[0]);
        }
    }

    protected function searchStringConstrain($query, $column, $operator, $values)
    {
        foreach ($values as &$v) {
            if ($operator == 'contains') {
                $v = "%{$v}%";
            }
            else {
                $v = ($operator == 'starts_with') ? "{$v}%" : "%{$v}";
            }
        }
        $pathList = explode('.', $column);
        $column = array_pop($pathList);
        $relation = implode('.', $pathList);

        if ($relation) {
            if (count($values) > 1) {
                return $query->whereHas($relation, function ($query) use ($column, $values) {
                    foreach ($values as $i => $v) {
                        $query->where($column, 'like', $v, $i > 0 ? 'or' : 'and');
                    }
                });
            }
            else {
                return $query->whereHas($relation, function ($query) use ($column, $values) {
                    $query->where($column, 'like', $values[0]);
                });
            }
        }
            
        if (count($values) > 1) {
            return $query->where(function ($query) use ($column, $values) {
                foreach ($values as $i => $v) {
                    $query->orWhere($column, 'like', $v, $i > 0 ? 'or' : 'and');
                }
            });
        }
        else {
            return $query->where($column, 'like', $values[0]);
        }
    }

    protected function inSetConstrain($query, $column, $operator, $values)
    {
        $function = 'where'.studly_case($operator);
        $pathList = explode('.', $column);
        $column = array_pop($pathList);
        $relation = implode('.', $pathList);

        if ($relation) {
            return $query->whereHas($relation, function ($query) use ($function, $column, $values) {
                $query->$function($column, $values);
            });
        }

        return call_user_func_array([$query, $function], [$column, $values]);
    }

    protected function nullConstrain($query, $column, $operator)
    {
        $function = $operator == 'is_null' ? 'whereNull' : 'whereNotNull';
        $pathList = explode('.', $column);
        $column = array_pop($pathList);
        $relation = implode('.', $pathList);

        if ($relation) {
            return $query->whereHas($relation, function ($query) use ($function, $column) {
                $query->$function($column);
            });
        }

        return call_user_func_array([$query, $function], [$column]);
    }
}