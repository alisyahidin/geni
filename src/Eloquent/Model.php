<?php /*** Bismillahirrahmanirrahim ***/

namespace Pusaka\Geni\Eloquent;

use Pusaka\Tanur\Eloquent\Model as BaseModel;

/**
 * Model
 */
class Model extends BaseModel
{
    use UtilizeUuidTrait;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::boot();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    final public function getRouteKeyName()
    {
        return 'uuid';
    }
}
